# recipe-app-api-proxy

NGINX proxy app for recipe api

## Usage

## Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to fwd requests to (Default : `APP`)
* `APP_PORT` - Port of the app to fwd requests to (Default : `9000`)

